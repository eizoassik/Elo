
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--下面这meta意思是告知浏览器的宽度是设备的宽度，缩放值为1.0-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>我的</title>
<link rel="stylesheet" href="style/index.css"></link>
<link rel="stylesheet" href="style/msg.css"></link>
<link rel="stylesheet" href="style/personal.css"></link>
<link rel="stylesheet" href="style/responsive.css"></link>
<link rel="stylesheet" href="style/animate.css"></link>
<!--下面这个js文件是为了兼容IE的媒体查询而准备的-->
<script src="js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="js/jquery-2.1.1.min.js""></script>
<script type="text/javascript" src="js/msg.js"></script>
<script type="text/javascript" src="js/headroom.min.js"></script>
<script type="text/javascript" src="js/headroom.js"></script>

</head>
<body>
<div id="header">
	<ul>
		<li><a href='index.php' title="访问首页，看看新鲜事物">YunFORM</a></li>
		<li><a href='create.php' title="创建你的新表单">创建</a></li>
		<li><a href='#' id="register-msg">注册</a>/<a href='#' id="login-msg">登录</a></li>		<li><a href='personal.php' title="查看已建立的表单">我的</a></li>
		<li class="search">
	       	<div class='search'>
                <form id="search" action="#">
                    <input class="search-text" type="text"  placeholder="search" name="q"></input>
                </form>
			</div>
		</li>
		<li  class='justify-helper'></li>
	</ul>
    <div class='search-mobile search'>
        <form id="search" action="#">
            <input class="search-text" type="text"  placeholder="输入你要找的组织或活动名" name="q"></input>
        </form>
    </div>
</div>

<div id='my-release' class="section">
	<div class="section-header">
        <h2>我发布的</h2>
        <div class="h2-line">
        </div>
    </div>
    <div class="section-body">
        <div class="card my-release" > 
            <a href='#'>
                <div class='form-op'>
                    <a href="#"><input class='btn red' type="button" value='编辑'/></a>
                    <a href="manage.php"><input class='btn blue' type="button" value='管理'/></a>
                    <a href="#"><input class='btn green' type="button" value='查看'/></a>
                </div>
                <div class='form-status'>
                    <img src="images/form-status-on.png" alt="已下架" />
                </div>
                <div class="img-holder">
                    <div class="fader">
                        <img src="images/2.jpg" alt="" />
                    </div>
                    <div class="form-name">
                        浙大某个社团网站纳新报名表
                    </div>
                    <div class="img-counter">
                        <div class="counter">
                            <span class="time-left">还有两天</span>
                            <span class="written">14次</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="card my-release" > 
            <a href='#'>
                <div class='form-op'>
                    <a href="#"><input class='btn red' type="button" value='编辑'/></a>
                    <a href="manage.php"><input class='btn blue' type="button" value='管理'/></a>
                    <a href="#"><input class='btn green' type="button" value='查看'/></a>
                </div>
                <div class='form-status'>
                    <img src="images/form-status-off.png" alt="已下架" />
                </div>
                <div class="img-holder">
                    <div class="fader">
                        <img src="images/1.jpg" alt="" />
                    </div>
                    <div class="form-name">
                        浙大某个社团网站纳新报名表
                    </div>
                    <div class="img-counter">
                        <div class="counter">
                            <span class="time-left">还有两天</span>
                            <span class="written">14次</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="card my-release" > 
            <a href='#'>
                <div class='form-op'>
                    <a href="#"><input class='btn red' type="button" value='编辑'/></a>
                    <a href="manage.php"><input class='btn blue' type="button" value='管理'/></a>
                    <a href="#"><input class='btn green' type="button" value='查看'/></a>
                </div>
                <div class='form-status'>
                    <img src="images/form-status-wait.png" alt="已下架" />
                </div>
                <div class="img-holder">
                    <div class="fader">
                        <img src="images/1.jpg" alt="" />
                    </div>
                    <div class="form-name">
                        浙大某个社团网站纳新报名表
                    </div>
                    <div class="img-counter">
                        <div class="counter">
                            <span class="time-left">还有两天</span>
                            <span class="written">14次</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="card my-release create-new" > 
            <a href="create.php">
                <div class="img-holder">
                    <b>+</b>
                    <p>点此创建新表</p>
                </div>
            </a>	
        </div>
    <!-- 这里是用来使元素左端对齐的 -->
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
    </div>
</div>
<div id='my-fill' class="section">
	<div class="section-header">
        <h2>我填写的</h2>
        <div class="h2-line">
        </div>
    </div>
    <div class="section-body">
        <div class="card my-fill" > 
            <a href='#'>
                <div class="img-holder">
                    <div class="fader">
                        <img src="images/1.jpg" alt="" />
                    </div>
                    <div class="form-name">
                        浙大某个社团网站纳新报名表
                    </div>
                    <div class="img-counter">
                        <div class="counter">
                            <span class="time-left">还有两天</span>
                            <span class="written">14次</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <!-- 这里是用来使元素左端对齐的 -->
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
    </div>
</div>
<div id='my-browse' class="section">
	<div class="section-header">
        <h2>我浏览的</h2>
        <div class="h2-line">
        </div>
    </div>
    <div class="section-body">
        <div class="card my-browse" > 
            <a href='#'>
                <div class="img-holder">
                    <div class="fader">
                        <img src="images/1.jpg" alt="" />
                    </div>
                    <div class="form-name">
                        浙大某个社团网站纳新报名表
                    </div>
                    <div class="img-counter">
                        <div class="counter">
                            <span class="time-left">还有两天</span>
                            <span class="written">14次</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <!-- 这里是用来使元素左端对齐的 -->
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
        <div class='card left-fix'>&nbsp;</div>
    </div>
</div>


<div class='msg'>
    <div class='msg-border'>
        <div class='msg-content'>			
        </div>
    </div>
</div>

﻿<div id='footer'>
	<div id='footer-logo'>
		YunFORM
	</div>
	<div id='footer-info'>
		<p>版权所有 YunFORM小组 2014-2014</p>
		<p>浙ICP备05074421号 Copyright ? 2014-2014</p>
		<p>设计制作:Lu | Liang | Su</p>
		<p>技术指导:Li | Yu</p>
		<p>友情链接：CC98 | 飘渺水云间 | 缘网 | NexusHD | 浙大学习网</p>
	</div>
	<div id='footer-contact'>
		<ul>
			<li>建议反馈</li>
			<li>关于本站</li>
			<li><a id="contact-msg">联系方式</a></li>
			<li><a id="work-msg">任务流程</li>
			<li>合作服务</li>
		</ul>
	</div>
	<div class='justify-helper'></div>
</div>
<script>
(function() {
   $("header").headroom({
  "tolerance": 5,
  "offset": 205,
  "classes": {
    "initial": "animated",
    "pinned": "bounceInDown",
    "unpinned": "bounceOutUp"
  }
});

// to destroy
$("#header").headroom("destroy");
}());
</script>
</body>
</html>